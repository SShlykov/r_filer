defmodule RFilerTest do
  use ExUnit.Case
  doctest RFiler

  setup do
    Application.ensure_all_started(:r_filer)
  end

  describe "работа с файлами" do
    setup do
      on_exit(&cleanup/0)

      %{
        filename: "test",
        text: "secret",
        extension: "txt"
      }
    end

    test "создание и чтение файла", %{filename: filename, text: text, extension: extension} do
      RFiler.write_file(text, filename, extension)

      assert File.read!("#{filename}.#{extension}") |> is_binary()
    end

    test "чтение файла с удалением", %{filename: filename, text: text, extension: extension} do
      RFiler.write_file(text, filename, extension)
      RFiler.read_file_and_remove("#{filename}.#{extension}")

      assert {:error, :enoent} == File.read("#{filename}.#{extension}")
    end

    test "проверка количества записей", %{filename: filename, text: text, extension: extension} do
      RFiler.write_file(text, filename, extension)

      assert {:ok, 2} == RFiler.count_logs("#{filename}.#{extension}")
      RFiler.write_file(text, filename, extension)
      assert {:ok, 3} == RFiler.count_logs("#{filename}.#{extension}")
    end
  end

  defp cleanup do
    File.rm!("test.txt")
  rescue
    e -> {:error, e}
  end
end
