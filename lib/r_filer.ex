defmodule RFiler do
  @moduledoc """
  Documentation for `RFiler`.
  """
  use Rustler, otp_app: :r_filer, crate: :filer

  @spec write_file(binary(), binary(), binary()) :: binary()
  @spec read_file(binary()) :: {:ok, binary()}
  @spec read_file_and_remove(binary()) :: {:ok, binary()}
  @spec count_logs(binary()) :: {:ok, number()}
  @spec add(integer(), integer()) :: integer()

  def write_file(_data, _filename, _type \\ "txt"), do: error()
  def read_file(_filename), do: error()
  def read_file_and_remove(_filename), do: error()
  def count_logs(_filename), do: error()
  def add(_a, _b), do: error()

  defp error, do: :erlang.nif_error(:nif_not_loaded)
end
