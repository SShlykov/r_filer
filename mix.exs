defmodule RFiler.MixProject do
  use Mix.Project

  def project do
    [
      app: :r_filer,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      compilers: Mix.compilers(),
      rustler_crates: [filer: []],
      test_coverage: [tool: ExCoveralls],
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {RFiler.Application, []}
    ]
  end

  defp deps do
    [
      {:excoveralls, "~> 0.10", only: :test},
      {:rustler, git: "git://github.com/rusterlium/rustler.git", branch: "master", sparse: "rustler_mix"},
    ]
  end
end
