use std::fs::{OpenOptions};
use std::io::{Write};

pub fn wfile(string: String, path: String) -> String {
    let mut file = OpenOptions::new().create(true).append(true).open(path.clone()).expect("Unable to open");

    match file.write(format!("\n{}", string).as_bytes()) {
        Err(why) => panic!("couldn't create {}: {}", path, why),
        Ok(_res) => (),
    }

    path
}