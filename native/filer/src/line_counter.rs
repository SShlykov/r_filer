use std::io;
use std::io::{BufRead, BufReader};

pub fn count_lines<R: io::Read>(handle: R) -> Result<usize, io::Error> {
    let reader = BufReader::with_capacity(1024 * 32, handle);

    Ok(reader.lines().fold(0, |sum, _| sum + 1))
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_count_lines() {
        let f: &[u8] = b"some text\nwith\nfour\nlines\n";
        assert_eq!(count_lines(f).unwrap(), 4);
    }
}