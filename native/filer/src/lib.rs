pub mod line_counter;
pub mod writer;
use std::io::Read;
use std::fs::{File};
use std::io::{BufReader};
use line_counter::count_lines;
use writer::wfile;

rustler::atoms! {
    ok,
    error,
    not_exist,
    cant_read,
    not_deleted,
}

rustler::init! {"Elixir.RFiler", [add, write_file, read_file, read_file_and_remove, count_logs]}

#[rustler::nif]
fn read_file(filename: String) -> (rustler::Atom, String) {
    let mut buffer = String::new();
    
    let file = File::open(filename).expect("omg, no file");
    let mut buf_reader = BufReader::new(file);
    buf_reader.read_to_string(&mut buffer).expect("omg, file unreadable");

    (ok(), buffer)
}

#[rustler::nif]
fn read_file_and_remove(filename: String) -> (rustler::Atom, String) {
    let mut buffer = String::new();
    
    let file = File::open(filename.clone()).expect("omg, no file");
    let mut buf_reader = BufReader::new(file);
    buf_reader.read_to_string(&mut buffer).expect("omg, file unreadable");
    
    match std::fs::remove_file(filename) {
        Err(_why)  => (error(), "not_deleted".to_string()),
        Ok(_file)  => (ok(), buffer),
    }    
}

#[rustler::nif]
fn write_file(string: String, filename: String, file_type: String) -> (rustler::Atom, String) {
    let res: String = wfile(string, format!("{}.{}", filename, file_type));

    (ok(), res)
}

#[rustler::nif]
fn count_logs(filename: String) -> (rustler::Atom, usize) {
    match std::fs::File::open(filename) {
        Err(_why) => (error(), 0),
        Ok(file)  => (ok(), count_lines(file).unwrap()),
    }
}    

#[rustler::nif]
fn add(num1: i64, num2: i64) -> (rustler::Atom, i64) {
    (ok(), num1 + num2)
}
